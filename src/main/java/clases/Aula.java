package clases;

public class Aula {
	private String id;
	private int capacidad;
	private String edificio;
	private String nombre;
	private boolean habilitado;
	private String tipo;
	
	public Aula(String id, int capacidad, String edificio,String nombre,boolean habilitado,String tipo) {
		id = id;
		capacidad = capacidad;
		edificio= edificio;
		nombre = nombre;
		habilitado = habilitado ;
		tipo = tipo ;
	}

	//Metodos Get de los atributos
	
	public String get_id() {
		return id;
	}

	public int get_capacidad() {
		return capacidad;
	}
	
	public String get_edificio() {
		return edificio;
	}

	public String get_nombre() {
		return nombre;
	}

	public boolean is_habilitado() {
		return habilitado;
	}

	public String get_tipo() {
		return tipo;
	}

	@Override
	public String toString() {
		return "Aula :  \nid= " + id + "\nCapacidad= " + capacidad + "\nEdificio= " + edificio + "\nNombre=" + nombre
				+ "\nHabilitado =" + habilitado + "\nTipo=" + tipo ;
	}

}

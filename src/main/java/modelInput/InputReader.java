package modelInput;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class InputReader {
	
	public static boolean esArchivoVacio(File file) {
		try {
			FileReader archivo = new FileReader(file);
		    BufferedReader br = new BufferedReader(archivo);
		    String linea;
		    while ((linea = br.readLine()) != null) {
		    	return false;
		    }
	    } catch(IOException e){
	    	e.printStackTrace();    	
	    }
		return true;
	}
	
	public static Set<Object> findClass(String path) throws Exception{
		Set<Object> result = new HashSet<>();
		

		File file = new File(path);
		if (file.isDirectory() || file.exists()) {
			File[] list = file.listFiles(); 
			for (File f : list) {
				if (!f.getName().endsWith(".class") || esArchivoVacio(f)) continue;
					String nombre = f.getName(); 		
					String archivo = nombre.substring(0,nombre.lastIndexOf(".")); 
					Class c = Class.forName("dto."+ archivo);
	
				result.add(c.newInstance());
			} 
		}
		return result;
	}

	
//	public static void main(String [] args) throws Exception { 		 
//		String path = System.getProperty("user.dir"); 		
//		String url = path + File.separator + "Projects";
//		findClass(url);
//		Set<Object> lista = findClass(url);
//		for(Object  e : lista)
//		{
//			System.out.println(e.getClass());
//		}
//		System.out.println(lista.size());		
//		
//	}
}


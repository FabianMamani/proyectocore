package test;

import static org.junit.Assert.assertEquals;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import clases.Aula;

public class AulaTest {	
	public static Aula aula;
	
	@Before
	public void setup() {
		aula = new Aula("A001",40,"Ciencias","Algoritmos I",true,"Laboratorio");
	}
	
	@Test
	public void verificarCapacidad() {
		assertEquals(40,aula.get_capacidad());
	}
	
	@Test
	public void verificarID() {
		assertEquals("A001", aula.get_id());
	}
	
	@Test
	public void verificarEdificio() {
		assertEquals("Ciencias",aula.get_edificio());
	}
	
	@Test
	public void verificarNombre() {
		assertEquals("Algoritmos I", aula.get_nombre());
	}
	
	@Test
	public void verificarHabilitacion() {
		assertEquals(true, aula.is_habilitado());
	}
	
	@Test
	public void verificarTipo() {
		assertEquals("Laboratorio", aula.get_tipo());
	}
	
	@After
	public void quit() {
		Assume.assumeTrue((!aula.toString().isEmpty()));
		//System.out.println(aula.toString());
	}

}

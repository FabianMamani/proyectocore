package test;

import json.Json;

public class JsonTest {
	static Json json;
	static Json file;
	static Json fileEmpty;
	static Json fileIncomplete;
	
	@BeforeTest
	public void setup() {
		json = new Json();
		json.cargar(json);
		json.generarJson("./ArchivosJson/archivoPrueba.json");
		
		file = Json.leerJson("./ArchivosJson/archivoPrueba.json");
		
		fileEmpty =Json.leerJson("./ArchivosJson/empty.json");
		fileIncomplete =Json.leerJson("./ArchivosJson/incomplete.json");
	}
	
	@AfterTest
	public void teadown() {
		System.out.println("Test Finalizado.");
	}
	
}
